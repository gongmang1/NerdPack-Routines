NePCR = {}

local Parse = NeP.DSL.parse
local Fetch = NeP.Interface.fetchKey

--(function() return dynEval('tank.health < '..PeFetch('NePconfPriestDisc', 'FlashHealTank')) end)

function NePCR.dynEval(condition, spell)
	return Parse(condition, spell or '')
end

local function getUnitsAround(unit)
	local numberUnits = 0
	for i=1,#NeP.OM.unitEnemie do
		local Obj = NeP.OM.unitEnemie[i]
		if UnitAffectingCombat(unit) and NeP.Core.Distance(unit, Obj.key) <= 10 then
			numberUnits = numberUnits + 1
		end
	end
	return numberUnits
end

NeP.library.register('NePCR', {

	HolyNova = function(units)
		local minHeal = GetSpellBonusDamage(2) * 1.125
		local total = 0
		for i=1,#NeP.OM.unitFriend do
			local Obj = NeP.OM.unitFriend[i]
			if Obj.distance <= 12 then
				if max(0, Obj.maxHealth - Obj.actualHealth) > minHeal then
					total = total + 1
				end
			end
		end
		return total > units
	end,

	aDot = function(Spell, refreshAt, health, class)
		-- Check if we have the spell before anything else...
		if not IsUsableSpell(Spell) then return false end
		-- Dont need to fill everything
		if refreshAt == nil then refreshAt = 0 end
		if health == nil then health = 100 end
		if class == nil then class = 3 end

		local Spellname, Spellrank, Spellicon, SpellcastingTime, SpellminRange, SpellmaxRange, SpellID = GetSpellInfo(Spell)
		local SpellcastingTime = SpellcastingTime * 0.001
		
		-- If toggle is enabled, do automated
		if peConfig.read('button_states', 'NeP_ADots', false) then
			-- Iterate thru OM
			for i=1,#NeP.OM.unitEnemie do
				local Obj = NeP.OM.unitEnemie[i]
				-- Affecting combat or Dummy and is elite or above
				if (UnitAffectingCombat(Obj.key) or Obj.is == 'dummy') and Obj.class >= class then	
					-- Sanity checks
					if Obj.health < health
					and UnitCanAttack('player', Obj.key)
					and NeP.Core.Infront('player', Obj.key)
					and IsSpellInRange(Spellname, Obj.key)then
						-- Do we have the debuff and is it expiring?
						local _,_,_,_,_,_,debuffDuration = UnitDebuff(Obj.key, Spellname, nil, 'PLAYER')
						if not debuffDuration or  - GetTime() < refreshAt then
							-- Dont cast if the targedebuffDurationt is going to die
							if debuffDuration == nil then debuffDuration = 0 end
							if NeP.DSL.Conditions['ttd'](Obj.key) > (debuffDuration + SpellcastingTime)
							-- Instant spells should bypass
							or SpellcastingTime < 1 then
								-- set PEs parsed Target and return true
								NeP.Engine.ForceTarget = Obj.key
								return true
							end
						end
					end
				end
			end
		-- Fallback to single target if toggle is disabled.
		else
			local _,_,_,_,_,_,debuffDuration = UnitDebuff('target', Spellname, nil, 'PLAYER')
			-- If Dont have the debuff
			if not debuffDuration or  - GetTime() < refreshAt then
				if debuffDuration == nil then debuffDuration = 0 end
				-- Sanity Checks
				if IsSpellInRange(Spellname, 'target')
				and NeP.Core.Infront('player', 'target')
				--[[and NeP_isElite('target')]] then
					-- Dont cast if the targedebuffDurationt is going to die
					if NeP.DSL.Conditions['ttd']('target') > (debuffDuration + SpellcastingTime)
					-- Instant spells should bypass
					or SpellcastingTime < 1 then
						return true
					end
				end
			end
		end
		return false
	end,

	SAoEObject = function(Units, Distance)
		local UnitsAroundObject = {}
		for i=1,#NeP.OM.unitEnemie do
			local Obj = NeP.OM.unitEnemie[i]
			if UnitAffectingCombat(Obj.key) and Obj.distance <= Distance then
				UnitsAroundObject[#UnitsAroundObject+1] = {
					unitsAround = getUnitsAround(Obj.key),
					key = Obj.key
				}
			end
		end
		table.sort(UnitsAroundObject, function(a,b) return a.unitsAround < b.unitsAround end)
		if UnitsAroundObject[1].unitsAround > Units then
			-- set PEs parsed Target and return true
			NeP.Engine.ForceTarget = UnitsAroundObject[1].key
			return true
		end
		return false
	end

})